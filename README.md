# oa.finder

## Description
Bielefeld University Library is developing the [oa.finder](https://finder.open-access.network) as part of the BMBF-funded joint project “[open-access.network](https://open-access.network/ueber-uns/oa-network)”. The oa.finder offers two search options: The search for journals that appear directly online and the search for academic publishers that publish open access books (monographs, edited volumes, conference proceedings, etc.).

The oa.finder/journal aggregates bibliographic data on scientific journals, information on open access, subject discipline and impact. In addition, information on APCs/BPCs, financing options via transformative agreements, open-access flat-fee contracts and publication funds of German academic institutions can be researched. For this publicly available data sources are used for the most part, which are compiled, processed and merged. The journal metadata of the [Electronic Journals Library](https://ezb.ur.de/) are the basic source for the currently about 57,000 journals in the oa.finder. It is enriched with information from [OpenAPC](https://openapc.net/), [DOAJ](https://doaj.org/), [Scopus](https://www.scopus.com/) and the [Journal Checker Tool](https://journalcheckertool.org/) among others. The source code for the compilation of the journal metadata and the documentation thereof is published here as open source. It includes neither the source data nor all the steps done after updating, such as building the index, creating the actual website for the oa.finder and so on.

In addition to the journal metadata, the oa.finder incorporates information on transformative aka read&publish agreements and OA flat-fee agreements. Transformative agreements are negotiated between institutions (libraries, national and regional consortia) and publishers in which former subscription expenditures are repurposed to support open access publishing. The oa.finder re-uses and amends public data on transformative agreements, which are relevant for German scientific institution from the [Journal Checker Tool](https://journalcheckertool.org/transformative-agreements/). Data is regularly updated and provided via this [Google Spreadsheet](https://docs.google.com/spreadsheets/d/1ODfgjX_atXQcMYf1hWry9DjLbHkiPCi1MYdgjPRPmHI/edit#gid=25145951) and licensed under [CC0](http://creativecommons.org/publicdomain/zero/1.0/).

Finally, oa.finder's information on open access services (like publication funds) and policies of German scientific institutions is taken from the [oa.atlas](https://open-access.network/services/oaatlas) of the [Open-Access-Büro Berlin](http://www.open-access-berlin.de/).

Disclaimer: The Bielefeld University Library takes no responsibility for the completeness, accuracy and currentness of the data and codes. In no event shall the Bielefeld University Library be liable for any damages, lost profits, and business interruption arising out of or in connection with the use or impossibility to use this service.

## License
The source code for compiling the journal metadata for the oa.finder is licensed under the GNU Affero General Public License v3.0.

## Creators
Dr. Nina Schönfelder (maintainer), Anna Dönecke (contributor)

## Installation
Clone this repository in a clean R working directory (R version 4.2.1 and RStudio 2024.04.0+735 is installed on my machine under Windows Windows 10 Pro). Create a new folder "data_input" for the all the source data that you have to download yourself (the URLs can be found in the R-files but maybe outdated), a new folder "data_temp"  for saving intermediate results and a new folder "data_out" to save the final results in csv-/txt-files. Install all necessary R libraries. 

`R-Code`
- contains set of R scripts used to build and update the data for the oa.finder

`data_input`
- forder for data that serves as input for the scripts
- i.e. downloaded files and files created for use in the scripts
  - automatically retrieved files or manually downloaded files from websites
  - intellectually compiled lists with, for example, publishers that need to be filtered out
- a lot of the files here have timestamps in their name and are not overwritten with each update
- they make it possible to trace back older versions

`data_output`
- folder for files that store results from the scripts
- i.e. files that store data that has been processed, enriched, manipulated in the scripts (which in some cases, serve as input for the scripts again)
- here you write the file `Enriched_EZB_metadata.csv`, the final result in which the processed data of all scripts is merged into

`data_temp`
- folder to store RData files with results of individual scripts
- files get overwritten each time a script is executed
- they serve as backup during update process and are used to restore results into R environment
- they are also used to load processed version of input data where new processing of data would be redundant because source is no longer updated

You need a DeepL account to translate keyword from German to English, a Scopus account to get the Scopus source list and the citation metrics, a "Web of Science" account to get the WoS Master List, and an EZB account to get the journal metadata (a very similar file can be downloaded [here](https://ezb.ur.de/services/titlelist.phtml?collection_id=journals) without log-in). Please pay attention to the terms and conditions of the data provider.




## Data sources and their use

**Basis structure/bibliographical metadata**

- _EZB title list_
  - comprehensive list of journals that serves as basis
  - bibliographical metadata and data on accessibility of titles
- _ZDB API_ for language information (as there is the ZDB ID in the EZB but no language metadata)

**Identification and/or removal of questionary titles**

- _Beall's list_ with predatory journals and publishers
- _DOAJ lists_ of journals withdrawn and falsely claiming to be indexed
- _Hijacked Journals Checker Tool_ with illegitimately 'cloned' journals
- _Mirror Journals list_ from Jülich

**Identification of OA status of journals**

- Bielefeld _DOAG list_ (Diamond Open Access Journals Germany) [no longer updated]
- ROAD (Directory of Open Access Scholarly Resources)
- _Platinum OA list_ from Swiss project PLATO [seemingly no longer updated]
- _DOAJ journal metadata_ (as all journals indexed there are, at least, Gold OA)
- _Transformative Journals Public Data_ from Journal Checker Tool to identify hybrid journals that are PlanS-compliant [outdated soon?]

**Enrichment with info on publishing**

- _DOAJ journal metadata_ for information such as list price APC, copyrights, PID, review and publication process, DOAJ Seal etc.
- _OpenAPC_ for cost information on paid fees
- Publishers' _websites with list prices_ of hybrid journals

**Enrichment with rankings/metrics**

- _VHB Rating_ 
- Handelsblatt _VWL Ranking_
- _Medline_ indexation (National Library of Medicine's bibliographical database)
- _Scopus Source list_ indexation
- _Web of Science Core Collection_ and _Journal Citation Reports_ indexation
- _Scopus metrics_, i.e. Scholarly Output, CiteScore, SNIP, SJR

**Manipulate and process data**

- _Exchange rates_ from European Central Bank
  - conversion of price/cost information to EUR
- _ISSN list_ mapping linking and 'normal' ISSNs from ISSN Center/ISSN Portal
  - unique identification of journals
  - matching data from different sources

## Sources and their scripts

| Source | Corresponding script |
|---|---|
| ISSN-to-ISSN-L-table from ISSN Center | [data import ISSN_ISSN-L_table.R](https://gitlab.ub.uni-bielefeld.de/nina.schoenfelder/oa.finder/-/blob/main/R-code/data%20import%20ISSN_ISSN-L_table.R)
| Foreign Exchange Rates from ECB | [data import FXR.R](https://gitlab.ub.uni-bielefeld.de/nina.schoenfelder/oa.finder/-/blob/main/R-code/data%20import%20FXR.R)
| DOAJ journal metadata | [data import DOAJ journal metadata.R](https://gitlab.ub.uni-bielefeld.de/nina.schoenfelder/oa.finder/-/blob/main/R-code/data%20import%20DOAJ%20journal%20metadata.R)
| predatory journals and publishers from Beall's list | [data import bealls lists.R](https://gitlab.ub.uni-bielefeld.de/nina.schoenfelder/oa.finder/-/blob/main/R-code/data%20import%20bealls%20lists.R)
| DOAJ journals withdrawn (old and new), journals falsely claiming to be in DOAJ | [data import questionary journals DOAJ.R](https://gitlab.ub.uni-bielefeld.de/nina.schoenfelder/oa.finder/-/blob/main/R-code/data%20import%20questionary%20journals%20DOAJ.R)
| EZB journal metadata | [data import EZB journal metadata.R](https://gitlab.ub.uni-bielefeld.de/nina.schoenfelder/oa.finder/-/blob/main/R-code/data%20import%20EZB%20journal%20metadata.R)
| APCs from OpenAPC | [data import APCs from OpenAPC.R](https://gitlab.ub.uni-bielefeld.de/nina.schoenfelder/oa.finder/-/blob/main/R-code/data%20import%20APCs%20from%20OpenAPC.R)
| MEDLINE | [data import MEDLINE.R](https://gitlab.ub.uni-bielefeld.de/nina.schoenfelder/oa.finder/-/blob/main/R-code/data%20import%20MEDLINE.R) 
| ZDB API | [data import ZDB-Katalog.R](https://gitlab.ub.uni-bielefeld.de/nina.schoenfelder/oa.finder/-/blob/main/R-code/data%20import%20ZDB-Katalog.R)
| DOAG | [data import DOAG.R](https://gitlab.ub.uni-bielefeld.de/nina.schoenfelder/oa.finder/-/blob/main/R-code/data%20import%20DOAG.R)
| VHB-Rating | [data import VHB.R](https://gitlab.ub.uni-bielefeld.de/nina.schoenfelder/oa.finder/-/blob/main/R-code/data%20import%20VHB.R)
| Platinum OA journals Switzerland | [data import PlatinumOACH.R](https://gitlab.ub.uni-bielefeld.de/nina.schoenfelder/oa.finder/-/blob/main/R-code/data%20import%20PlatinumOACH.R)
| VWL-Zeitschriftenliste | [data import VWL-Zeitschriftenliste.R](https://gitlab.ub.uni-bielefeld.de/nina.schoenfelder/oa.finder/-/blob/main/R-code/data%20import%20VWL-Zeitschriftenliste.R)
| Mirror Journals | [data import Mirror Journals.R](https://gitlab.ub.uni-bielefeld.de/nina.schoenfelder/oa.finder/-/blob/main/R-code/data%20import%20Mirror%20Journals.R)
| Transformative Journals data cOAlition S | [data import Transformative Journals data cOAlition S.R](https://gitlab.ub.uni-bielefeld.de/nina.schoenfelder/oa.finder/-/blob/main/R-code/data%20import%20Transformative%20Journals%20data%20cOAlition%20S.R)
| WoS Core Collection, WoS Journal Citation Reports | [data import WoS Master List.R](https://gitlab.ub.uni-bielefeld.de/nina.schoenfelder/oa.finder/-/blob/main/R-code/data%20import%20WoS%20Master%20List.R)
| Scopus source list, CiteScore | [data import Scopus source list and metrics.R](https://gitlab.ub.uni-bielefeld.de/nina.schoenfelder/oa.finder/-/blob/main/R-code/data%20import%20Scopus%20source%20list%20and%20metrics.R)
| ROAD | [data import ROAD.R](https://gitlab.ub.uni-bielefeld.de/nina.schoenfelder/oa.finder/-/blob/main/R-code/data%20import%20ROAD.R)
| Publishers' websites with price lists for hybrid journals | [data import APC price lists.R](https://gitlab.ub.uni-bielefeld.de/nina.schoenfelder/oa.finder/-/blob/main/R-code/data%20import%20APC%20price%20lists.R)



## Compiling and updating data: roadmap

### General structure

The go-to script for a complete update is [update.R](https://gitlab.ub.uni-bielefeld.de/nina.schoenfelder/oa.finder/-/blob/main/R-code/update%20data.R) that sources all the other scripts in a specific order you should only deviate from with careful thought. A few of the scripts need some manual work, i.e. downloading and filenaming, before running. You will find a corresponding comment in the update script and further directions in the respective script. In case the original data source is no longer updated, the script loads the last version from the corresponding RData file. When using those RData files, keep in mind that in many scripts, the linking issn is added and that the RData files are based on the then used version of the matching table between issns and linking issns!

Finally, with [merge data.R](https://gitlab.ub.uni-bielefeld.de/nina.schoenfelder/oa.finder/-/blob/main/R-code/merge%20data.R) all the data from the individual scripts (which in several cases, use the results of other scripts and are thus already interconnected) will be merged, new variables will be derived and added, and the overall result will be written into a csv file used to build the oa.finder).

### Step by step

##### 1. functions.R

[The script](https://gitlab.ub.uni-bielefeld.de/nina.schoenfelder/oa.finder/-/blob/main/R-code/functions.R) defines _functions_ that are used throughout the scripts. They provide checks for invalid ISSNs, for column comparison between new input files and the version the code expects and for the environment to be setup accordingly so your update runs smoothly.

##### 2. data import ISSN_ISSN-L_table.R

[The script](https://gitlab.ub.uni-bielefeld.de/nina.schoenfelder/oa.finder/-/blob/main/R-code/data%20import%20ISSN_ISSN-L_table.R) processes ISSN lists automatically downloaded from the ISSN Portal. The result is a _table mapping ISSNs with Linking ISSNs_. The table is used to assign the linking ISSNs to records that are often only referred to by their print or electronic ISSN in the input data.

##### 3. data import FXR.R

[The script](https://gitlab.ub.uni-bielefeld.de/nina.schoenfelder/oa.finder/-/blob/main/R-code/data%20import%20FXR.R) automatically imports the recent _rates for currency conversion_ from the European Central Bank. The table is used in some of the following scripts in order to convert APC values into EUR.

##### 4. data import DOAJ journal metadata.R

[The script](https://gitlab.ub.uni-bielefeld.de/nina.schoenfelder/oa.finder/-/blob/main/R-code/data%20import%20DOAJ%20journal%20metadata.R) automatically downloads _journal metadata from DOAJ_ which already are in pretty good shape and do not need a lot of manipulation. Mainly, the linking ISSN is added and the APC values are converted to EUR and there is a column added to indicate whether the journal qualifies as scholar-led.

##### 5. data import bealls lists.R

[The script](https://gitlab.ub.uni-bielefeld.de/nina.schoenfelder/oa.finder/-/blob/main/R-code/data%20import%20bealls%20lists.R) processes _Beall's list of predatory publishers and standalone journals_. It requires that you first manually save the html content from Beall's website in a specific manner and do some manual modifications as described in the script (e.g. we decided to remove http://www.frontiersin.org/ from the list). The script then basically creates a vector containg the domains of the journals and publishers by splitting URLs contained in the html. The vector is later used to filter the data downloaded from the EZB. It is likely that you can simply load the version from the RData file because the amendments to the original Beall's list seems to be maintained only occationally.

##### 6. data import questionary journals DOAJ.R

[The script](https://gitlab.ub.uni-bielefeld.de/nina.schoenfelder/oa.finder/-/blob/main/R-code/data%20import%20questionary%20journals%20DOAJ.R) automatically downloads and processes the list of _journals withdrawn from DOAJ and the list of journals falsely claiming to be DOAJ-indexed_. In particular, the latest version of the list with removed journals is downloaded, combined with the old, no longer updated list and then filtered for journals that were withdrawn due to non-adherence to best practices. To the resulting list as well as to the list of 'false' journals the linking ISSN is added.

##### 7. data import EZB journal metadata.R

[The script](https://gitlab.ub.uni-bielefeld.de/nina.schoenfelder/oa.finder/-/blob/main/R-code/data%20import%20EZB%20journal%20metadata.R) needs you to first manually download the table with all records from the EZB. The script imports these records, filters them in multiple steps and adds the linking ISSN. The result is an _extensive list of journals including information on their accessibility serving as the basis structure for the oa.finder_. As there are nearly 100k records to be processed, this takes several minutes (you may want to run the script on the TSP server). First, the set is reduced to those titles that are active without a moving wall and have a ISSN. Secondly, titles which are not scientific journals open for submissions (e.g. annual reports, trade magazines) are removed. Third, predatory journals are identified and filtered out using Beall's list. In a fourth step, the Retraction Watch Hijacked Journal Checker list is imported in order to check for hijacked journals in the EZB data set (however, they are not automatically removed). After the linking ISSN has been added, further filtering steps follow. Questionable journals are removed based on the DOAJ lists, titles exclusively accessible against payment as well as doublets are filtered out. Finally, the keywords provided by the EZB are brought into shape for further processing.

##### 8. data import APCs from OpenAPC.R

[The script](https://gitlab.ub.uni-bielefeld.de/nina.schoenfelder/oa.finder/-/blob/main/R-code/data%20import%20APCs%20from%20OpenAPC.R) automatically downloads and processes the most recent data from _OpenAPC_. There are a few corrections for individual journals but mainly the scripts adds some calculated values for journals such as the mean APC.

##### 9. data import MEDLINE.R

[The script](https://gitlab.ub.uni-bielefeld.de/nina.schoenfelder/oa.finder/-/blob/main/R-code/data%20import%20MEDLINE.R) needs you to first download the dataset of all _Medline records_ in XML format. It then imports the data and creates a dataframe with the linking ISSNs of the records for later use.

##### 10. data import ZDB-Katalog.R

[The script](https://gitlab.ub.uni-bielefeld.de/nina.schoenfelder/oa.finder/-/blob/main/R-code/data%20import%20ZDB-Katalog.R) completely runs automatically but can take up to several hours. This is due to the fact that it sends queries to the _ZDB API_ with a rather costly loop iterating over all ZDB numbers taken from the processed EZB data and extracting _language_ information from the retrieved JSON data. You may want to run it on the TSP server while working on other things. The result is a dataframe consisting of the ZDB number and the URI to the Library of Congress ISO639-2 Languages vocabulary indicating the language of a journal.

##### 11. data import DOAG.R

As the _Diamond Open Access Journals Germany_ list published is no longer maintained, the corresponding [script](https://gitlab.ub.uni-bielefeld.de/nina.schoenfelder/oa.finder/-/blob/main/R-code/data%20import%20DOAG.R) does not need to be executed. Load the last version stored in a RData file instead. The list is later used to identify journals not raising an APC via their linking ISSN.

##### 12. data import VHB.R

[The script](https://gitlab.ub.uni-bielefeld.de/nina.schoenfelder/oa.finder/-/blob/main/R-code/data%20import%20VHB.R) automatically imports the _VHB rating_ and performs some corrections on false ISSNs.


##### 13. data import PlatinumOACH.R

The list of _Diamond OA journals in Switzerland_ imported and processed by this [script](https://gitlab.ub.uni-bielefeld.de/nina.schoenfelder/oa.finder/-/blob/main/R-code/data%20import%20PlatinumOACH.R) is no longer updated. In the script, the list is imported and then enriched by linking ISSNs. The results is later used, as is the DOAG list from Bielefeld, to identify journals not raising an APC. Simply load the last version from the RData file as specified in the update script.

##### 14. data import VWL-Zeitschriftenliste.R

[The script](https://gitlab.ub.uni-bielefeld.de/nina.schoenfelder/oa.finder/-/blob/main/R-code/data%20import%20VWL-Zeitschriftenliste.R) imports the _Handelsblatt VWL Ranking_. After a lot of individual corrections, the linking ISSN is added.

##### 15. data import Mirror Journals.R

[The script](https://gitlab.ub.uni-bielefeld.de/nina.schoenfelder/oa.finder/-/blob/main/R-code/data%20import%20Mirror%20Journals.R) imports the _Jülich list with mirror journals_ and adds linking ISSNs to it. As the list is no longer updated, load the last version from the corresponding RData file instead of executing the script.

##### 16. data import Transformative Journals data cOAlition S.R

[The script](https://gitlab.ub.uni-bielefeld.de/nina.schoenfelder/oa.finder/-/blob/main/R-code/data%20import%20Transformative%20Journals%20data%20cOAlition%20S.R) automatically downloads the latest version of the _Transformative, i.e. Plan S compliant, Journals list_ from the Journal Checker Tool and adds the linking ISSN.

##### 17. data import WoS Master List.R

[The script](https://gitlab.ub.uni-bielefeld.de/nina.schoenfelder/oa.finder/-/blob/main/R-code/data%20import%20WoS%20Master%20List.R), after manual download, imports the current _Web of Science Core Collection_ Master Journal List as well as the list of the _Journal Citation Reports_. It adds the linking ISSN and binds the tables into one data frame for later use.

##### 18. data import Scopus source list and metrics.R

[The script](https://gitlab.ub.uni-bielefeld.de/nina.schoenfelder/oa.finder/-/blob/main/R-code/Scopus_corrections.R), both lists are merged and the linking ISSN is added.

##### 19. data import ROAD.R

[The script](https://gitlab.ub.uni-bielefeld.de/nina.schoenfelder/oa.finder/-/blob/main/R-code/data%20import%20ROAD.R) imports the ROAD data to determine OA status of journals.

##### 20. data import APC price lists.R

[The script](https://gitlab.ub.uni-bielefeld.de/nina.schoenfelder/oa.finder/-/blob/main/R-code/data%20import%20APC%20price%20lists.R) imports and processes _APC price lists for hybrid journals_ of several publishers, adds a column with the price converted to Euro and binds them all together into one data frame to which the linking ISSNs are added. Some of the price lists are imported as xlsx or csv files, some are directly taken from the website. In any case, careful supervision is required.

##### 21. translate via DeepL.R

[The script](https://gitlab.ub.uni-bielefeld.de/nina.schoenfelder/oa.finder/-/blob/main/R-code/translate%20via%20DeepL.R) is used to _translate the German keywords from the EZB and the English keywords from DOAJ_ to German and English respectively. It divides both rather large lists into portions that have to be manually uploaded to as well as downloaded from the BIS DeepL instance and are then again imported in the script.

##### 22. merge data.R

[The scripts](https://gitlab.ub.uni-bielefeld.de/nina.schoenfelder/oa.finder/-/blob/main/R-code/compile%20score%20for%20diamond,%20scholar-led%20preferred.R), is added. Finally, the result is written into file.

### What to look out for

There is a number of aspects in the scripts that are more prone to errors than others and/or need some attention. You may want to look out for the following points or simply keep them in mind when working with the code.

#### Interdependencies among scripts

- The scripts are interrelated, with most of them building on the results of others.
- If the update process is interrupted, essential objects might no longer be available in the environment.
- You can reload these objects from RData files stored in the `data_temp` directory, which are created at the end of each script. (If R Studio does not automatically load them from your R project.)
- However, be cautious not to mix up different versions of objects across scripts.

#### Accurate filtering of EZB data

- The EZB includes a broad range of periodicals, not just scientific journals, and it catalogs access options ("Zugänge") rather than journals, resulting in many duplicates and non-relevant entries.
- Therefore, the EZB data requires thorough and careful filtering.
- This filtering process may require periodic maintenance or at least close attention when running the EZB script.
- Use the `ezb_stat` object, which stores the number of records at each filtering step, to verify the filtering accuracy.

#### Consistency in data structure and column names of new inputs

- New input files might have different column names or order compared to the original version the code was designed for.
- While checks have been implemented, at least for tabular data, but they might not work for all scenarios.
- Pay particular attention when importing hierarchically structured data, such as XML.
- If needed, you can look up the expected columns/fields in the versioned files located in `data_input`.
- In some cases, column names are passed through from the original input file to the merging of all sources, and hence, to rendering the final output.
- This means that if the input file has changed column name(s), you should consider renaming them in the respective script right after reading the file into R (or be extremely attentive when adjusting code in all related scripts). 

#### Handling file names in download and read commands

- File naming conventions vary across the scripts.
- Some file names are dynamically generated based on the system date, while others are hardcoded as strings.
- Always verify the file naming before running the code to avoid any errors.

#### Processing data directly taken from websites

- This is particularly relevant with respect to importing price lists of hybrid journals.
- Only run these scripts under careful supervision and be prepared to have to modify the code.




