####################################################################################
### Script for resolving problem with the linking ISSNs
### Version 20220823.ISSN-to-ISSN-L.txt
####################################################################################


# Download zip-file from https://www.issn.org/wp-content/uploads/2014/03/issnltables.zip
# Extract zip-file
## Script for reading text files and performing basic corrections. Run code row by row:
#ATTETION: Adjust number of rows.
ISSN_to_ISSN.L<-read.delim(file="data_input\\20220926.ISSN-to-ISSN-L.txt", nrows = 2500000, colClasses = "character") 

# Function to replace flawed linking ISSN with the (online or print) ISSN of the journal (default), or any other ISSN-L (to specify as character).
issnlchange<-function (issn, issnl=issn){ 
  hv<-sapply(ISSN_to_ISSN.L$ISSN, "==",issn);
  for (i in 1:length(hv))  if (hv[i]==TRUE) ISSN_to_ISSN.L$ISSN.L[i]<<-issnl
  }

issnlchange("2058-5888")   # Environmental Epigenetics with flawed linking ISSN
issnlchange("2337-4713")  # Historia: Jurnal Pendidikan Sejarah FKIP UM Metro with flawed linking ISSN
issnlchange("2477-9660")     # Publicaciones en Ciencias y Tecnología with flawed linking ISSN
issnlchange("1294-8535","0219-5259")   # Advances in Complex Systems with flawed linking ISSN

ISSN_to_ISSN.L<-rbind(ISSN_to_ISSN.L,c("1834-4461","0029-8077")) # Add E-ISSN for "Oceania"
# One L-ISSN for two journals published by the same society: "Chemical Engineering Research and Design" and "Process Safety and Environmental Protection"
issnlchange("1744-3563","0263-8762") # Replace L-ISSN with the P-ISSN for "Chemical Engineering Research and Design"
issnlchange("0263-8762")             # Replace L-ISSN with the P-ISSN "Chemical Engineering Research and Design"
# One L-ISSN for two journals published by the publisher
issnlchange("1793-9623")  #  "International Journal of Modeling, Simulation, and Scientific Computing
# One L-ISSN for two journals published by the publisher
issnlchange("2096-1650") # "Journal of Geodesy and Geoinformation Science" 

