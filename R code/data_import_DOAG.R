#########################################################################
# Download and read "Diamond Open Access Journals Germany"  (DOAG)      #
# Bruns, A., Taubert, N. C., Cakir, Y., Kaya, S., & Beidaghi, S. (2022).#
# Diamond Open Access Journals Germany (DOAG) Version 1.1.              #
# Bielefeld University.  https://doi.org/10.4119/unibi/2965484          #
#########################################################################

download.file("https://pub.uni-bielefeld.de/download/2965484/2965489/DOAG_1_1.csv", destfile="data_input\\diamond_oa_journals_germany.csv") # download file 

DOAG<-read.csv("data_input\\diamond_oa_journals_germany.csv", na.strings="",encoding = "UTF-8",colClasses = "character")
DOAG<-subset(DOAG, select=c(ISSN_L, TITLE)) # select relevant columns
DOAG<-unique(DOAG) # make records unique
summary(as.factor(DOAG$ISSN_L))# OK

colnames(DOAG)<-paste("DOAG",colnames(DOAG),sep = "_") 


doag<-subset(DOAG, ((JOURNAL_IN_OJS=="1" |JOURNAL_IN_PMC=="1") & JOURNAL_IN_DOAJ=="0" & JOURNAL_IN_ROAD=="0"))

             
