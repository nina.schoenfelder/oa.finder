#########################################################################
# Import information on a journal's full-text language from ZDB-Katalog
# See ZDB Help: https://zdb-katalog.de/help.xhtml
# and https://zeitschriftendatenbank.de/services for API etc.
################################################################

# check environment and load objects if necessary
if (!exists("check_df", where = globalenv())) {
  source("R-code/functions.R")
}
check_df("ezb", mapping)
check_package(c("httr", "car", "jsonlite"))

# Extract ZDB numbers from list created in "R-code\\data import EZB journal metadata.R"
ZDB.Nummer <- (ezb$EZB_ZDB.Nummer) # Extract ZDB.Nummer
summary(is.na(ZDB.Nummer)) # 26 ZDB numbers are missing
ZDB.Nummer <- subset(ZDB.Nummer, !is.na(ZDB.Nummer)) # delete records without ZDB.number

# Identify records with invalid ZDB numbers
ZDB_pattern <- "^[0123456789]+[0123456789]+[0123456789]+[0123456789]+[0123456789]+[0123456789]+[0123456789]+-+[0123456789xX]$"
Ups <- grep(ZDB_pattern, ZDB.Nummer, value = TRUE, invert = TRUE)
print(Ups)

# Split characters for mutiple ZDB.Nummer and use only the first ZDB.Nummer
ZDB.Nummer_list <- strsplit(ZDB.Nummer, split = ";")
ZDB.Nummer <- (sapply(1:length(ZDB.Nummer_list), function(x) {ZDB.Nummer_list[[x]][1]})) # use first ZDB.Nummer

# Create schema for URLs for automatic download from ZDB
URL_request <- paste0("https://ld.zdb-services.de/data/", ZDB.Nummer, ".jsonld")

# Prepare vector for results to be stored in
results <- vector("list")  # prepare list from content from ZDB

# Automatic download of ZDB entries
# Duration: approximately 45 min for 10.000 titles
for (i in 1:length(ZDB.Nummer)) {response <- GET(URL_request[i]) # send URL
  warn_for_status(response, task = "ZDB API for language") # Converts http errors to R errors or warnings
  if (http_error(response) == FALSE) {
    data <- fromJSON(rawToChar(response$content)) # get answer and convert from json to list
    results$language[i] <- paste0(data$`http://purl.org/dc/terms/language`[[length(data$`@id`)]][[1]], collapse = "***")  # extract full-text language, one or multiple
    results$ID[i] <- data$`http://purl.org/dc/elements/1.1/identifier`[[length(data$`@id`)]][[1]][[2]][[1]] # exact the ZDB ID from the response for control issues
    results$Nummer[i] <- ZDB.Nummer[i] # assign ZBD ID from request
  }
  else {
    results$language[i] <- NA # If request failed
    results$ID[i] <- NA # If request failed
    results$Nummer[i] <- ZDB.Nummer[i] # assign ZBD ID from request
  }
}

# Convert results from queries above to data frame
ZDB <- as.data.frame(results)

# in case ZDB.Nummer was not made unique before
ZDB$unique <- !duplicated(ZDB)
ZDB <- subset(ZDB, ZDB$unique == TRUE, select = -unique)
ZDB$language <- car::recode(ZDB$language, "''=NA")

# Add data source as prefix to the columns
colnames(ZDB) <- paste("ZDB", colnames(ZDB), sep = "_")

# Save as RData file for temporary backup during update process
save(ZDB, file = "data_temp\\zdb.RData")

# Via dump download
# library(R.utils)

# Download ZDB-Titeldaten im Format RDF (JSON-LD)
# download.file("https://opendata:opendata@data.dnb.de/zdb_lds.jsonld.gz",  destfile = "data_input\\zdb_lds.jsonld.gz") # download file ZDB / Deutsche Nationalbibliothek
# https://data.dnb.de/opendata/zdb_lds.rdf.gz # XML Format 2 GB entpackt

# keep workspace clean
rm(ZDB.Nummer, ZDB_pattern, Ups, ZDB.Nummer_list, URL_request, results, i, response, data)
